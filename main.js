const cells = document.querySelectorAll('.cell');
const message = document.querySelector('.message');
const resetButton = document.querySelector('.reset-button');
let xTurn = true;

function handleClick(e) {
    const cell = e.target;
    const currentClass = xTurn ? 'X' : 'O';
    placeMark(cell, currentClass);
    if (checkWin(currentClass)) {
        endGame(false);
    } else if (isDraw()) {
        endGame(true);
    } else {
        swapTurns();
        setBoardHoverClass();
    }
}

function placeMark(cell, currentClass) {
    cell.textContent = currentClass;
}

function swapTurns() {
    xTurn = !xTurn;
}

function setBoardHoverClass() {
    board.classList.remove('X', 'O');
    if (xTurn) {
        board.classList.add('X');
    } else {
        board.classList.add('O');
    }
}

function checkWin(currentClass) {
    // Check for win logic
}

function isDraw() {
    return [...cells].every(cell => {
        return cell.textContent === 'X' || cell.textContent === 'O';
    });
}

function endGame(draw) {
    if (draw) {
        message.textContent = 'Draw!';
    } else {
        message.textContent = `${xTurn ? "X's" : "O's"} Wins!`;
    }
}

resetButton.addEventListener('click', () => {
    // Reset game logic
});

cells.forEach(cell => {
    cell.addEventListener('click', handleClick, { once: true });
});
